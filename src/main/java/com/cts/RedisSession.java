package com.cts;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@ComponentScan
@Controller
@Component
public class RedisSession {

	@RequestMapping(value="load")
	public void loadSession(HttpServletRequest request, HttpServletResponse response){
		System.out.println("inside load methods");
		request.getSession().setAttribute("username", request.getParameter("userName"));
		System.out.println(request.getSession().toString());
		//ModelAndView mav = new ModelAndView();
		//mav.setViewName("sessionView");
		//return mav;
	}
	
	}
