<!DOCTYPE html>
<html lang="en">
<head>
    <title>Session Attributes</title>
    
</head>
<body>
    <div >
        <h1>Try it</h1>

        <form action="${pageContext.request.contextPath}/load" method="get">
            <label for="userName">User Name</label>
            <input id="userName" type="text" name="userName"/>
            <input type="submit" value="Set Attribute"/>
        </form>
    </div>
</body>
</html>
